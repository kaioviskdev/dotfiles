#!/bin/bash

# Variables
country=Brazil
kbmap=br
output=DP-0
resolution=1920x1080
rate=200.00

# Options
aur_helper=true
install_ly=true
gen_xprofile=true

sudo timedatectl set-ntp true
sudo hwclock --systohc
sudo reflector -c $country -a 12 --sort rate --save /etc/pacman.d/mirrorlist

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload
# sudo virsh net-autostart default

if [[ $aur_helper = true ]]; then
    cd /tmp
    git clone https://aur.archlinux.org/yay.git
    cd yay/;makepkg -si --noconfirm;cd
fi

# Install packages
sudo pacman -S xorg librewolf nitrogen lxappearance pcmanfm flameshot thunderbird alacritty picom obs-studio steam lutris qbittorrent \
vlc ffmpeg bitwarden xterm rofi sddm lxsession lynx curl wget mpv tmux gparted htop font-manager bc newsflash discord xmonad xmonad-contrib \
xmobar urxvt 

yay -S spotify-edge stremio  

sudo systemctl enable sddm

# Install fonts
sudo pacman -S --needed --noconfirm dina-font tamsyn-font ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex \
ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code \
ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts \
cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts \
noto-fonts-cjk noto-fonts-emoji wqy-zenhei lib32-fontconfig

yay -S bdf-unifont ttf-ms-fonts

# Pull Git repositories and install
cd /tmp
repos=( "dmenu" "dwm" "dwmstatus" "st" "slock" )
for repo in ${repos[@]}
do
    git clone git://git.suckless.org/$repo
    cd $repo;make;sudo make install;cd ..
done

# XSessions and dwm.desktop
if [[ ! -d /usr/share/xsessions ]]; then
    sudo mkdir /usr/share/xsessions
fi

cat > ./temp << "EOF"
[Desktop Entry]
Encoding=UTF-8
Name=Dwm
Comment=Dynamic window manager
Exec=dwm
Icon=dwm
Type=XSession
EOF
sudo cp ./temp /usr/share/xsessions/dwm.desktop;rm ./temp

# .xprofile
if [[ $gen_xprofile = true ]]; then
cat > ~/.xprofile << EOF
setxkbmap $kbmap
nitrogen --restore
xrandr --output $output --mode $resolution --rate $rate
EOF
fi

printf "\e[1;32mDone! you can now reboot.\e[0m\n"
