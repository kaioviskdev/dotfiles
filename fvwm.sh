#!/bin/sh

# Variables
country=Brazil
kbmap=br
output=DP-0
resolution=1920x1080

# Mirror refresh
sudo timedatectl set-ntp true
sudo hwclock --systohc
sudo reflector -c $country -a 6 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syyyu

# Firewall setup
sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

# Install yay
cd /tmp
git clone https://aur.archlinux.org/yay.git
cd yay/;makepkg -si --noconfirm;cd

# Install packages
sudo pacman -S xorg-server xorg-apps xorg-xinit xdg-utils alacritty firefox-developer-edition nitrogen lxappearance picom pcmanfm dmenu hsetroot 

# Install display manager
cd /tmp
git clone --recurse-submodules https://github.com/fairyglade/ly
cd /ly;make;make run;
make install installsystemd

# Optional dependencies 
sudo pacman -S --needed asciidoctor libfontconfig-dev libfreetype6-dev libfribidi-dev libncurses5-dev libpng-dev libreadline-dev librsvg-dev libsm-dev libxcursor-dev libxext-dev libxi-dev libxpm-dev sharutils

# Install FVWM3 
cd $HOME
git clone https://github.com/fvwmorg/fvwm3.git 
cd fvwm3/
./autogen.sh && ./configure && make

# Install fonts
sudo pacman -S --needed ttf-liberation noto-fonts ttf-roboto tex-gyre-fonts ttf-fira-mono ttf-fira-code ttf-jetbrains-mono noto-fonts-cjk noto-fonts-emoji
yay -S ttf-ms-win10-auto bdf-unifont 

# Xinit config
cat > ~/.xinitrc << EOF
setxkbmap $kbmap
nitrogen --restore
xrandr --output $output --mode $resolution
EOF

# Enable services
sudo systemctl enable ly
