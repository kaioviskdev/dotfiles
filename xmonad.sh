#!/bin/sh

# Variables
country=Brazil
kbmap=br
output=DP-0
resolution=1920x1080
rate=200

# Mirror refresh
sudo timedatectl set-ntp true
sudo hwclock --systohc
sudo reflector -c $country -a 6 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syy

# Firewall setup
sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

# Install paru
cd /tmp
git clone https://aur.archlinux.org/paru.git
cd paru/;makepkg -si --noconfirm;cd

# Install packages
sudo pacman -S xorg firefox nitrogen lxappearance pcmanfm flameshot thunderbird alacritty picom obs-studio steam lutris qbittorrent \
vlc ffmpeg bitwarden xterm rofi sddm lxsession lynx curl wget mpv tmux gparted htop font-manager bc newsflash discord stack xorg-xinit xorg-xdm \
xdm-archlinux

paru -S spotify-edge stremio  

# Install fonts
sudo pacman -S --needed --noconfirm dina-font tamsyn-font ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex \
ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code \
ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts \
cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts \
noto-fonts-cjk noto-fonts-emoji wqy-zenhei lib32-fontconfig

paru -S bdf-unifont ttf-ms-fonts

# Xprofile config
cat > ~/.xprofile << EOF
# Keyboard layout
setxkbmap $kbmap

# Background setup
nitrogen --restore

# Compositor setup
picom -f

# Resolution config
xrandr --output $output --mode $resolution --rate $rate
EOF

# Configure Xmonad
mkdir -p ~/.config/xmonad && cd ~/.config/xmonad

git clone https://github.com/xmonad/xmonad
git clone https://github.com/xmonad/xmonad-contrib

sudo stack upgrade
stack init && stack install

cat > ~/.config/xmonad/xmonad.hs << EOF
import XMonad

main :: IO ()
main = xmonad $ def 
    {
        terminal    =   "alacritty",
        modMask     =   mod4Mask
    }
EOF

# Enable services
sudo systemctl enable xdm-archlinux.service